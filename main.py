from typing import List, Dict

from fastapi import FastAPI, HTTPException
import mook

app = FastAPI()


def preprocess_text(text: str) -> List[str]:
    text = text.lower()
    text = " ".join(word for word in text.split() if not word.isspace())
    return text.split()


def search_tutelas(tutela_buscar: str) -> List[Dict[str, str]]:
    preprocessed_tipo_tutela = preprocess_text(tutela_buscar)
    indices = {}
    filter = ["violencia", "estafa", "alimentos", "salud", "pension", "medicinas"]

    for tutela in mook.tutela:
        words = tutela["resumen"].split()
        for word in words:
            if word in filter:
                if word in indices:
                    indices[word].append(tutela)
                else:
                    indices[word] = [tutela]

    # Concatenate the preprocessed_tipo_tutela with a '+' delimiter
    preprocessed_tipo_tutela_str = '+'.join(preprocessed_tipo_tutela)

    if preprocessed_tipo_tutela_str not in indices:
        return []

    return indices[preprocessed_tipo_tutela_str]


@app.get("/", summary="Página principal")
def root():
    return {"Servicio": "Estructuras de Datos"}

@app.get("/api/v1/search", summary="Buscar tutelas por palabra clave")
def search(palabra: str) -> Dict[str, List[Dict[str, str]]]:
    index = search_tutelas(palabra)
    if not index:
        return {"message": f"No tutelas found for keyword '{palabra}'"}
    return {"Tutelas encontradas": index}
